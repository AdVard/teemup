from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.models import User
import sys



def menu():
    return [
        {'link':"/", 'name':'Университеты'},
        {'link': "/Сlubs", 'name': 'Клубы'},
        {'link': "/Positions", 'name': 'Позиции'},
        {'link': "/Athletes", 'name': 'Спортсмены'},
        #{'link': "/", 'name': 'О проекте'},
        {'link': "registration/RegisOrEnter/", 'name': 'Регистрация/Вход'},

    ]

def web():
    return [
                {
                    #facebook
                    'link':'',
                    'class':'feather feather-facebook fea icon-sm fea-social',
                    'path':'M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z',
                    'x':'-1000',
                    'y':'-1000',
                    'width':'-1000',
                    'height':'-1000',
                    'rx':'-1000',
                    'ry':'-1000',
                    'x1': '-1000',
                    'y1': '-1000',
                    'x2': '-1000',
                    'y2': '-1000',
                    'cx': '-1000',
                    'cy': '-1000',
                    'r': '-1000',
                 },
                {
                    #instagram
                    'link': '',
                    'class':"feather feather-instagram fea icon-sm fea-social",
                    'path':"M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z",
                    'x': '2',
                    'y': '2',
                    'width': '20',
                    'height': '20',
                    'rx': '5',
                    'ry': '5',
                    'x1': '17.5',
                    'y1': '6.5',
                    'x2': '17.51',
                    'y2': '6.5',
                    'cx': '',
                    'cy': '',
                    'r': '',
                },
                {
                    #tweeter
                    'link': '',
                    'class':"feather feather-twitter fea icon-sm fea-social",
                    'path':"M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z",
                    'x':'-1000',
                    'y':'-1000',
                    'width':'-1000',
                    'height':'-1000',
                    'rx':'-1000',
                    'ry':'-1000',
                    'x1': '-1000',
                    'y1': '-1000',
                    'x2': '-1000',
                    'y2': '-1000',
                    'cx': '-1000',
                    'cy': '-1000',
                    'r': '-1000',
                },
                {
                    #in
                    'link': '',
                    'class':"feather feather-linkedin fea icon-sm fea-social",
                    'path':"M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z",
                    'x': '2',
                    'y': '9',
                    'width': '4',
                    'height': '12',
                    'rx':'-1000',
                    'ry':'-1000',
                    'x1': '-1000',
                    'y1': '-1000',
                    'x2': '-1000',
                    'y2': '-1000',
                    'cx':'4',
                    'cy':'4',
                    'r':'2',
                }

    ]

def company():
    return [
        {
            'link':'',
            'name':' About us'
        },
        {
            'link': '',
            'name': ' Services'
        },
        {
            'link': '',
            'name': ' Team'
        },
        {
            'link': '',
            'name': ' Pricing'
        },
        {
            'link': '',
            'name': ' Project'
        },
        {
            'link': '',
            'name': ' Careers'
        },
        {
            'link': '',
            'name': ' Blog'
        },
        {
            'link': '',
            'name': ' Login'
        },
    ]

def usfull_links():
    return [
        {
            'link':'',
            'name':'Terms of Services'
        },
        {
            'link': '',
            'name': 'Privacy Policy'
        },
        {
            'link': '',
            'name': 'Documentation'
        },
        {
            'link': '',
            'name': 'Changelog'
        },
        {
            'link': '',
            'name': 'Components'
        },
    ]

def uni(request):
    Univer=[
    {'png_link':'/static/Unevers_files/campus.jpg',
    'name':'ABC',
    'disc':'qwertyuiop',
    'count_clubs': '2',
    'count_position':'123',
    'un_link':'https://www.ox.ac.uk/'},
    {'png_link': '/static/Unevers_files/campus.jpg',
     'name': 'ABC',
     'disc': 'qwertyuiop',
     'count_clubs': '2',
     'count_position': '123',
     'un_link': 'https://www.ox.ac.uk/'},
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'disc': 'qwertyuiop',
         'count_clubs': '2',
         'count_position': '123',
         'un_link': 'https://www.ox.ac.uk/'},

    ]
    data={'Univer':Univer}
    data['menu']=menu()
    data['web']=web()
    data['company']=company()
    data['usfull_links']=usfull_links()

    return render(request,'Univers.html',data)

def enter(request):
    data={}
    data['menu'] = menu()
    data['web']=web()
    data['company']=company()
    data['usfull_links']=usfull_links()


    return render(request,'registration/RegisOrEnter.html',data)

def clubs(request):
    Club = [
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'disc': 'qwertyuiop',
         'count_clubs': '2',
         'count_position': '123',
         'un_link': 'https://www.ox.ac.uk/'},
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'disc': 'qwertyuiop',
         'count_clubs': '2',
         'count_position': '123',
         'un_link': 'https://www.ox.ac.uk/'}
    ]
    data = {'Club': Club}
    data['menu']=menu()
    data['web']=web()
    data['company']=company()
    data['usfull_links']=usfull_links()

    return render(request,'Clubs.html',data)

def sports(requst):
    Sports = [
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'type': 'Бегун',
         'sport_link': 'https://www.ox.ac.uk/',
         'ex':'3',
         'type_tags':[
             'a','b','c','a','b','c','a','b','c','a','b','c'
         ],
         },
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'type': 'Бегун',
         'sport_link': 'https://www.ox.ac.uk/',
         'ex': '3',
         'type_tags': [
             'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c'
         ],
         },
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'type': 'Бегун',
         'sport_link': 'https://www.ox.ac.uk/',
         'ex': '3',
         'type_tags': [
             'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c'
         ],
         },
        {'png_link': '/static/Unevers_files/campus.jpg',
         'name': 'ABC',
         'type': 'Бегун',
         'sport_link': 'https://www.ox.ac.uk/',
         'ex': '3',
         'type_tags': [
             'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c'
         ],
         },
        ]
    data = {'Sports': Sports}
    data['menu']=menu()
    data['web']=web()
    data['company']=company()
    data['usfull_links']=usfull_links()


    return render(requst,"Sportsmen.html",data)

def pos(requst):
    data={}
    data['menu']=menu()

    data['pos']=[{
        'photo':'\static\Position_files\position.jpg',
        'pos_link':'https://www.ox.ac.uk/',
        'pos_name':'ABC',
        'step':'Степендия',
        'logo':'\static\Position_files\position.jpg',
        'club_name':'Vik',
        'un_name':'Ox',
        'tags':[
            {'name':'a'},
            {'name':'b'},
            {'name':'c'}
        ]
    }
    ]
    data['web']=web()
    data['company']=company()
    data['usfull_links']=usfull_links()


    return render(requst,"Position.html",data)


class LoginView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        context = {}
        context['web'] = web()
        context['company'] = company()
        context['usfull_links'] = usfull_links()
        if request.method == 'POST':
            email = request.POST['email_aut']
            password = request.POST['pass']
            users = authenticate(request, username=email, password=password)
            if users is not None:
                login(request, users)
                redirect('')
            else:
                context['error'] = "Логин или пароль неправильные"
        return render(request, "registration/RegisOrEnter.html", context)

class RegisterView(TemplateView):
    template_name = "registration/RegisOrEnter/"

    def dispatch(self, request, *args, **kwargs):
        context = {}
        context['web'] = web()
        context['company'] = company()
        context['usfull_links'] = usfull_links()
        if request.method == 'POST':
            username = request.POST.get('username')
            sys.stderr.write()
            sys.stderr.write()
            sys.stderr.write()
            sys.stderr.write(str(username))
            sys.stderr.write()
            sys.stderr.write()
            sys.stderr.write()
            email = request.POST.get('email')
            password = request.POST.get('password')
            password2 = request.POST.get('password2')

            if password == password2:
                User.objects.create_user(username, email, password)
                return render(request, 'registration/tem_auth/')

        return render(request, self.template_name)



class ProfilePage(TemplateView):
    template_name = "registration/profile.html"

